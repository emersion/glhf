module gitlab.freedesktop.org/emersion/glhf

go 1.19

require (
	git.sr.ht/~emersion/go-scfg v0.0.0-20231211181832-0b4e72d8ec3c
	gopkg.in/irc.v4 v4.0.0
)

require golang.org/x/time v0.3.0 // indirect
