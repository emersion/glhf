package main

import (
	"fmt"
	"reflect"
	"testing"
)

func TestFindGitlabRefs(t *testing.T) {
	projects = []gitlabProject{
		{shorthand: ""},
		{shorthand: "foo"},
		{shorthand: "emersion/bar"},
	}

	tests := []struct {
		text string
		refs []gitlabRef
	}{
		{
			text: "#42",
			refs: []gitlabRef{{Kind: gitlabRefIssue, ID: "42"}},
		},
		{
			text: "!42",
			refs: []gitlabRef{{Kind: gitlabRefMergeRequest, ID: "42"}},
		},
		{
			text: "see !42",
			refs: []gitlabRef{{Kind: gitlabRefMergeRequest, ID: "42"}},
		},
		{
			text: "have you seen !42?",
			refs: []gitlabRef{{Kind: gitlabRefMergeRequest, ID: "42"}},
		},
		{
			text: "also look at !42 if you have time",
			refs: []gitlabRef{{Kind: gitlabRefMergeRequest, ID: "42"}},
		},
		{
			text: "that's true (but also see !42)",
			refs: []gitlabRef{{Kind: gitlabRefMergeRequest, ID: "42"}},
		},
		{
			text: "foo#42",
			refs: []gitlabRef{{Kind: gitlabRefIssue, ID: "42", ProjectIndex: 1}},
		},
		{
			text: "emersion/bar#42",
			refs: []gitlabRef{{Kind: gitlabRefIssue, ID: "42", ProjectIndex: 2}},
		},
		{
			text: "see foo#42.",
			refs: []gitlabRef{{Kind: gitlabRefIssue, ID: "42", ProjectIndex: 1}},
		},
		{
			text: "baz#42",
			refs: nil,
		},
		{
			text: "#foo",
			refs: nil,
		},
		{
			text: "#42foo",
			refs: nil,
		},
		{
			text: "emersion/baz#42",
			refs: nil,
		},
		{
			text: "hi(#42",
			refs: []gitlabRef{{Kind: gitlabRefIssue, ID: "42"}},
		},
		{
			text: "#42)bar",
			refs: []gitlabRef{{Kind: gitlabRefIssue, ID: "42"}},
		},
		{
			text: "see #42 and !43",
			refs: []gitlabRef{
				{Kind: gitlabRefIssue, ID: "42"},
				{Kind: gitlabRefMergeRequest, ID: "43"},
			},
		},
		{
			text: "ef2ce1bc84bcc3a2bc4f24849a2dd86ac8e64531",
			refs: []gitlabRef{{Kind: gitlabRefCommit, ID: "ef2ce1bc84bcc3a2bc4f24849a2dd86ac8e64531"}},
		},
		{
			text: "ef2ce1bc84bcc3a2bc4f24849a2dd86ac8e645312",
			refs: nil,
		},
		{
			text: "ef2ce1bc",
			refs: []gitlabRef{{Kind: gitlabRefCommit, ID: "ef2ce1bc"}},
		},
		{
			text: "should be fixed by ef2ce1bc, no?",
			refs: []gitlabRef{{Kind: gitlabRefCommit, ID: "ef2ce1bc"}},
		},
		{
			text: "ef2ce1b",
			refs: nil,
		},
		{
			text: "ef2ce1bz",
			refs: nil,
		},
	}

	for i, tc := range tests {
		tc := tc // capture
		t.Run(fmt.Sprintf("[%v]", i), func(t *testing.T) {
			refs := findGitlabRefs(tc.text)
			if !reflect.DeepEqual(refs, tc.refs) {
				t.Errorf("findGitlabRefs(%q) = %#v, want %#v", tc.text, refs, tc.refs)
			}
		})
	}
}
