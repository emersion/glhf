package main

func isGitHash(s string) bool {
	if len(s) < 8 || len(s) > 40 {
		return false
	}
	for _, ch := range s {
		if (ch < '0' || ch > '9') && (ch < 'a' || ch > 'f') {
			return false
		}
	}
	return true
}
