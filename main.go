package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync/atomic"
	"time"

	"gopkg.in/irc.v4"
)

const usage = `usage: glhf [options...] <irc-url>

The IRC URL can be:

- irc.example.org
- irc.example.org:6697
- user@irc.example.org
- ircs://user:pass@irc.example.org
- ircs://irc.example.org/#foo
- irc+insecure://irc.example.org

Options:

`

type gitlabProject struct {
	rootURL   string
	id        int64
	shorthand string
}

var (
	gitlabToken string
	projects    []gitlabProject
	debug       bool
	ircChannel  atomic.Value // string

	ircClient *irc.Client
)

func main() {
	var (
		configFilename string
		flagCfg        Config
	)
	flag.StringVar(&configFilename, "config", "", "Configuration file")
	flag.StringVar(&flagCfg.Listen, "listen", "", "HTTP listening address")
	flag.StringVar(&flagCfg.GitlabToken, "gitlab-token", "", "GitLab webhook secret token")
	flag.BoolVar(&debug, "debug", false, "Enable debug logging")
	flag.Usage = func() {
		fmt.Fprint(flag.CommandLine.Output(), usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if len(flag.Args()) > 1 {
		flag.Usage()
		os.Exit(1)
	}
	flagCfg.Connect = flag.Arg(0)

	cfg := new(Config)
	if configFilename != "" {
		var err error
		cfg, err = loadConfig(configFilename)
		if err != nil {
			log.Fatalf("failed to load config file: %v", err)
		}
	}

	if flagCfg.Listen != "" {
		cfg.Listen = flagCfg.Listen
	}
	if flagCfg.GitlabToken != "" {
		cfg.GitlabToken = flagCfg.GitlabToken
	}
	if flagCfg.Connect != "" {
		cfg.Connect = flagCfg.Connect
	}

	if cfg.Listen == "" {
		cfg.Listen = "localhost:8080"
	}
	if cfg.Connect == "" {
		log.Fatalf("missing IRC URL to connect to")
	}

	gitlabToken = cfg.GitlabToken
	for _, projCfg := range cfg.GitlabProject {
		var url, shorthand string
		switch len(projCfg.Params) {
		case 1:
			url = projCfg.Params[0]
		case 2:
			shorthand, url = projCfg.Params[0], projCfg.Params[1]
		default:
			log.Fatalf("invalid gitlab-project directive")
		}

		project := findGitlabProject(url)
		project.shorthand = shorthand
		projects = append(projects, project)
	}

	ircChannel.Store("")

	createIRCClient(cfg.Connect)

	httpServer := http.Server{
		Addr:    cfg.Listen,
		Handler: http.HandlerFunc(handleHTTPWebHook),
	}
	log.Printf("HTTP server listening on %v", httpServer.Addr)
	if err := httpServer.ListenAndServe(); err != nil {
		log.Fatalf("failed to listen and serve on HTTP: %v", err)
	}
}

func createIRCClient(ircURL string) {
	if !strings.Contains(ircURL, "://") {
		ircURL = "ircs://" + ircURL
	}

	u, err := url.Parse(ircURL)
	if err != nil {
		log.Fatalf("failed to parse IRC URL: %v", err)
	}

	var defaultPort string
	switch u.Scheme {
	case "ircs":
		defaultPort = ":6697"
	case "irc+insecure":
		defaultPort = ":6667"
	default:
		log.Fatal("only ircs:// and irc+insecure:// are supported URL schemes")
	}

	addr := u.Host
	if _, _, err := net.SplitHostPort(u.Host); err != nil {
		addr += defaultPort
	}

	nick := "glhf"
	pass := ""
	if u.User != nil {
		nick = u.User.Username()
		pass, _ = u.User.Password()
	}

	if u.Fragment != "" {
		ircChannel.Store("#" + u.Fragment)
	}

	var conn net.Conn
	switch u.Scheme {
	case "ircs":
		tlsConfig := &tls.Config{
			NextProtos: []string{"irc"},
		}
		conn, err = tls.Dial("tcp", addr, tlsConfig)
	case "irc+insecure":
		conn, err = net.Dial("tcp", addr)
	}
	if err != nil {
		log.Fatalf("failed to connect to IRC server: %v", err)
	}

	ircClient = irc.NewClient(conn, irc.ClientConfig{
		Nick: nick,
		Pass: pass,
		User: nick,
		Name: nick,

		EnableISupport: true,
		EnableTracker:  true,

		PingFrequency: time.Minute,
		PingTimeout:   30 * time.Second,

		Handler: irc.HandlerFunc(handleIRCMessage),
	})
	go func() {
		if err := ircClient.Run(); err != nil {
			log.Fatalf("IRC error: %v", err)
		}
	}()
}

func findGitlabProject(projectURL string) gitlabProject {
	u, err := url.Parse(projectURL)
	if err != nil {
		log.Fatalf("failed to parse GitLab project URL: %v", err)
	}

	projectPath := strings.TrimPrefix(u.Path, "/")
	u.Path = ""
	rootURL := u.String()

	id, err := fetchGitlabProjectID(rootURL, projectPath)
	if err != nil {
		log.Fatalf("failed to fetch GitLab project: %v", err)
	}

	return gitlabProject{
		rootURL: rootURL,
		id:      id,
	}
}

func handleHTTPWebHook(w http.ResponseWriter, req *http.Request) {
	if gitlabToken != "" && req.Header.Get("X-Gitlab-Token") != gitlabToken {
		http.Error(w, "Invalid secret token", http.StatusForbidden)
		return
	}

	ch := ircChannel.Load().(string)
	if ch == "" {
		http.Error(w, "No IRC channel set", http.StatusInternalServerError)
		return
	}

	// TODO: handle tag and release events
	switch event := req.Header.Get("X-Gitlab-Event"); event {
	case "Push Hook":
		var data gitlabPushWebhook
		if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
			http.Error(w, "Failed to decode JSON body", http.StatusBadRequest)
			return
		}
		handlePushWebHook(&data, ch)
	case "Issue Hook":
		var data gitlabIssueWebhook
		if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
			http.Error(w, "Failed to decode JSON body", http.StatusBadRequest)
			return
		}
		handleIssueWebhook(&data, ch)
	case "Merge Request Hook":
		var data gitlabMergeRequestWebhook
		if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
			http.Error(w, "Failed to decode JSON body", http.StatusBadRequest)
			return
		}
		handleMergeRequestWebhook(&data, ch)
	case "Release Hook":
		var data gitlabReleaseWebhook
		if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
			http.Error(w, "Failed to decode JSON body", http.StatusBadRequest)
			return
		}
		handleReleaseWebhook(&data, ch)
	case "Wiki Page Hook":
		var data gitlabWikiPageWebhook
		if err := json.NewDecoder(req.Body).Decode(&data); err != nil {
			http.Error(w, "Failed to decode JSON body", http.StatusBadRequest)
			return
		}
		handleWikiPageWebhook(&data, ch)
	default:
		log.Printf("unhandled GitLab event %q", event)
	}
}

func handlePushWebHook(data *gitlabPushWebhook, ircChannel string) {
	var onBranch string
	if branch := strings.TrimPrefix(data.Ref, "refs/heads/"); branch != data.Project.DefaultBranch {
		onBranch = " on " + WithForeground(branch, ColorGreen)
	}

	commits := data.Commits
	if data.TotalCommitsCount > 5 {
		commits = commits[:4]
	}
	for _, commit := range commits {
		hash := WithForeground(commit.ID[:8], ColorOrange)
		ircClient.WriteMessage(&irc.Message{
			Command: "PRIVMSG",
			Params:  []string{ircChannel, hash + onBranch + " " + commit.Title},
		})
	}

	if data.TotalCommitsCount != len(commits) {
		link := WithForeground(fmt.Sprintf("%v/-/compare/%v...%v", data.Project.WebURL, data.Before, data.After), ColorCyan)
		text := fmt.Sprintf("(and %v more commits: %v)", data.TotalCommitsCount-len(commits), link)
		ircClient.WriteMessage(&irc.Message{
			Command: "PRIVMSG",
			Params:  []string{ircChannel, text},
		})
	}
}

func handleIssueWebhook(data *gitlabIssueWebhook, ircChannel string) {
	var verb string
	switch data.ObjectAttributes.Action {
	case "open", "":
		verb = "opened"
	case "close":
		verb = "closed"
	case "reopen":
		verb = "re-opened"
	default:
		return
	}

	user := WithForeground("@"+escapeNickname(ircChannel, data.User.Username), ColorGreen)
	ref := WithForeground(fmt.Sprintf("#%v", data.ObjectAttributes.IID), ColorOrange)
	link := WithForeground(data.ObjectAttributes.URL, ColorCyan)
	text := fmt.Sprintf("%v %v issue: %v %v (%v)", user, verb, ref, data.ObjectAttributes.Title, link)
	ircClient.WriteMessage(&irc.Message{
		Command: "PRIVMSG",
		Params:  []string{ircChannel, text},
	})
}

func handleMergeRequestWebhook(data *gitlabMergeRequestWebhook, ircChannel string) {
	var verb string
	switch data.ObjectAttributes.Action {
	case "open", "":
		verb = "opened"
	case "close":
		verb = "closed"
	case "reopen":
		verb = "re-opened"
	case "merge":
		verb = "merged"
	default:
		return
	}

	user := WithForeground("@"+escapeNickname(ircChannel, data.User.Username), ColorGreen)
	ref := WithForeground(fmt.Sprintf("!%v", data.ObjectAttributes.IID), ColorOrange)
	link := WithForeground(data.ObjectAttributes.URL, ColorCyan)
	text := fmt.Sprintf("%v %v merge request: %v %v (%v)", user, verb, ref, data.ObjectAttributes.Title, link)
	ircClient.WriteMessage(&irc.Message{
		Command: "PRIVMSG",
		Params:  []string{ircChannel, text},
	})
}

func handleReleaseWebhook(data *gitlabReleaseWebhook, ircChannel string) {
	if data.Action != "create" {
		return
	}

	link := WithForeground(data.URL, ColorCyan)
	text := fmt.Sprintf("New %v release: %v (%v)", data.Project.Name, data.Name, link)
	ircClient.WriteMessage(&irc.Message{
		Command: "PRIVMSG",
		Params:  []string{ircChannel, text},
	})
}

func handleWikiPageWebhook(data *gitlabWikiPageWebhook, ircChannel string) {
	var verb string
	switch action := data.ObjectAttributes.Action; action {
	case "create", "update", "delete":
		verb = action + "d"
	default:
		return
	}

	user := WithForeground("@"+escapeNickname(ircChannel, data.User.Username), ColorGreen)
	link := WithForeground(data.ObjectAttributes.URL, ColorCyan)
	text := fmt.Sprintf("%v %v wiki page %v (%v)", user, verb, data.ObjectAttributes.Title, link)
	ircClient.WriteMessage(&irc.Message{
		Command: "PRIVMSG",
		Params:  []string{ircChannel, text},
	})
}

func escapeNickname(ch, s string) string {
	if s == "" {
		return s
	}

	chState := ircClient.Tracker.GetChannel(ch)
	if chState == nil {
		return s
	}

	for nick := range chState.Users {
		if strings.EqualFold(nick, s) {
			// Insert a zero-width space to avoid highlighting folks
			return s[0:1] + "\u200B" + s[1:]
		}
	}
	return s
}

func handleIRCMessage(client *irc.Client, msg *irc.Message) {
	if debug {
		log.Printf("received: %v", msg)
	}
	switch msg.Command {
	case irc.RPL_WELCOME:
		log.Println("logged in to IRC")
		if ch := ircChannel.Load().(string); ch != "" {
			client.WriteMessage(&irc.Message{
				Command: "JOIN",
				Params:  []string{ch},
			})
		}
	case irc.ERR_PASSWDMISMATCH, irc.ERR_ERRONEUSNICKNAME, irc.ERR_NICKNAMEINUSE, irc.ERR_NICKCOLLISION:
		log.Fatalf("failed to login: %v", msg.Params[len(msg.Params)-1])
	case irc.ERR_NOSUCHCHANNEL, irc.ERR_TOOMANYCHANNELS, irc.ERR_BADCHANNELKEY, irc.ERR_BANNEDFROMCHAN, irc.ERR_CHANNELISFULL, irc.ERR_INVITEONLYCHAN, irc.ERR_BADCHANMASK:
		ch := ircChannel.Load().(string)
		if strings.EqualFold(ch, msg.Params[1]) {
			log.Fatalf("failed to join channel %q: %v", ch, msg.Params[len(msg.Params)-1])
		}
	case irc.RPL_ENDOFMOTD, irc.ERR_NOMOTD: // end of ISUPPORT
		if botMode, ok := client.ISupport.GetRaw("BOT"); ok {
			client.WriteMessage(&irc.Message{
				Command: "MODE",
				Params:  []string{client.CurrentNick(), "+" + botMode},
			})
		}
	case "JOIN":
		ch := msg.Params[0]
		if msg.Prefix.Name == client.CurrentNick() && ircChannel.CompareAndSwap("", ch) {
			log.Printf("selected IRC channel %q", ch)
		}
	case "PRIVMSG":
		ch := msg.Params[0]
		text := msg.Params[1]

		if len(projects) == 0 {
			break
		}

		if msg.Prefix != nil && strings.EqualFold(msg.Prefix.Name, client.CurrentNick()) {
			break
		}

		refs := findGitlabRefs(text)
		if len(refs) > 5 {
			refs = refs[:5]
		}
		for _, ref := range refs {
			project := projects[ref.ProjectIndex]
			switch ref.Kind {
			case gitlabRefIssue:
				issue, err := fetchGitlabIssue(project, ref.ID)
				if err != nil {
					log.Printf("failed to fetch GitLab issue #%v: %v", ref.ID, err)
					break
				}
				refText := WithForeground(ref.String(), ColorOrange)
				link := WithForeground(issue.WebURL, ColorCyan)
				text := fmt.Sprintf("%v %v (%v)", refText, issue.Title, link)
				client.WriteMessage(&irc.Message{
					Command: "PRIVMSG",
					Params:  []string{ch, text},
				})
			case gitlabRefMergeRequest:
				mr, err := fetchGitlabMergeRequest(project, ref.ID)
				if err != nil {
					log.Printf("failed to fetch GitLab merge request !%v: %v", ref.ID, err)
					break
				}
				refText := WithForeground(ref.String(), ColorOrange)
				link := WithForeground(mr.WebURL, ColorCyan)
				text := fmt.Sprintf("%v %v (%v)", refText, mr.Title, link)
				client.WriteMessage(&irc.Message{
					Command: "PRIVMSG",
					Params:  []string{ch, text},
				})
			case gitlabRefCommit:
				commit, err := fetchGitlabCommit(project, ref.ID)
				if err != nil {
					log.Printf("failed to fetch GitLab commit %v: %v", ref.ID, err)
					break
				}
				refText := WithForeground(commit.ID[:8], ColorOrange)
				link := WithForeground(commit.WebURL, ColorCyan)
				text := fmt.Sprintf("%v %v (%v)", refText, commit.Title, link)
				client.WriteMessage(&irc.Message{
					Command: "PRIVMSG",
					Params:  []string{ch, text},
				})
			}
		}
	}
}
