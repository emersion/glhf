# glhf

GitLab Hook Forwarder: forward GitLab events to IRC.

## Usage

    glhf [options...] ircs://<username>:<password>@<host>

See `-help` for a full list of options.

Configure your HTTP reverse proxy to forward requests to glhf, then register
a new WebHook in the GitLab project settings.

glhf can be set up with a confguration file as well via `-config`. Example:

```
listen localhost:8080
connect ircs://irc.libera.chat/#test
gitlab-token asdf
gitlab-project https://gitlab.freedesktop.org/wlroots/wlroots
gitlab-project wlr-protocols https://gitlab.freedesktop.org/wlroots/wlr-protocols
```

Adding explicit GitLab projects makes the bot expand references like `#42`.

## License

MIT
