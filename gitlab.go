package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"unicode"
)

type gitlabPushWebhook struct {
	Ref    string `json:"ref"`
	Before string `json:"before"`
	After  string `json:"after"`

	Project struct {
		Name          string `json:"name"`
		DefaultBranch string `json:"default_branch"`
		WebURL        string `json:"web_url"`
	} `json:"project"`

	Commits []struct {
		ID     string `json:"id"`
		Title  string `json:"title"`
		Author struct {
			Name string `json:"name"`
		} `json:"author"`
	} `json:"commits"`
	TotalCommitsCount int `json:"total_commits_count"`
}

type gitlabIssueWebhook struct {
	Project struct {
		Name string `json:"name"`
	} `json:"project"`
	User struct {
		Username string `json:"username"`
	} `json:"user"`
	ObjectAttributes struct {
		Action string `json:"action"`
		Title  string `json:"title"`
		IID    int64  `json:"iid"`
		URL    string `json:"url"`
	} `json:"object_attributes"`
}

type gitlabMergeRequestWebhook struct {
	Project struct {
		Name string `json:"name"`
	} `json:"project"`
	User struct {
		Username string `json:"username"`
	} `json:"user"`
	ObjectAttributes struct {
		Action string `json:"action"`
		Title  string `json:"title"`
		IID    int64  `json:"iid"`
		URL    string `json:"url"`
	} `json:"object_attributes"`
}

type gitlabReleaseWebhook struct {
	Name    string `json:"name"`
	Project struct {
		Name string `json:"name"`
	} `json:"project"`
	URL    string `json:"url"`
	Action string `json:"action"`
}

type gitlabWikiPageWebhook struct {
	User struct {
		Username string `json:"username"`
	} `json:"user"`
	ObjectAttributes struct {
		Action  string `json:"action"`
		Title   string `json:"title"`
		Message string `json:"message"`
		URL     string `json:"url"`
	} `json:"object_attributes"`
}

type gitlabRefKind int

const (
	gitlabRefIssue gitlabRefKind = iota + 1
	gitlabRefMergeRequest
	gitlabRefCommit
)

func (kind gitlabRefKind) String() string {
	switch kind {
	case gitlabRefIssue:
		return "issue"
	case gitlabRefMergeRequest:
		return "merge request"
	case gitlabRefCommit:
		return "commit"
	default:
		panic(fmt.Sprintf("unknown gitlabRefKind: %d", kind))
	}
}

func (kind gitlabRefKind) Prefix() string {
	switch kind {
	case gitlabRefIssue:
		return "#"
	case gitlabRefMergeRequest:
		return "!"
	default:
		return ""
	}
}

type gitlabRef struct {
	Kind         gitlabRefKind
	ID           string
	ProjectIndex int
}

func (ref *gitlabRef) String() string {
	s := projects[ref.ProjectIndex].shorthand
	if prefix := ref.Kind.Prefix(); prefix != "" {
		s += prefix
	} else if s != "" {
		s += " "
	}
	s += ref.ID
	return s
}

func findGitlabRefs(s string) []gitlabRef {
	words := strings.FieldsFunc(s, func(ch rune) bool {
		return unicode.IsSpace(ch) || strings.ContainsRune("()[]{},:;", ch)
	})

	var refs []gitlabRef
	m := make(map[gitlabRef]struct{})
	for _, word := range words {
		word = strings.TrimRight(word, ".!?")

		ref := parseGitlabRef(word)
		if ref == nil {
			continue
		}

		if _, dup := m[*ref]; dup {
			continue
		}

		refs = append(refs, *ref)
		m[*ref] = struct{}{}
	}
	return refs
}

func parseGitlabRef(s string) *gitlabRef {
	if isGitHash(s) {
		projectIndex := projectIndexByShorthand("")
		if projectIndex < 0 {
			return nil
		}
		return &gitlabRef{
			Kind:         gitlabRefCommit,
			ID:           s,
			ProjectIndex: projectIndex,
		}
	}

	i := strings.IndexAny(s, "#!")
	if i < 0 {
		return nil
	}

	projectName := s[:i]
	kindCh := s[i]
	id := s[i+1:]

	projectIndex := projectIndexByShorthand(projectName)
	if projectIndex < 0 {
		return nil
	}

	var kind gitlabRefKind
	switch kindCh {
	case '#':
		kind = gitlabRefIssue
	case '!':
		kind = gitlabRefMergeRequest
	}

	validID := len(id) > 0
	for _, ch := range id {
		if !unicode.IsDigit(ch) {
			validID = false
			break
		}
	}
	if !validID {
		return nil
	}

	return &gitlabRef{
		Kind:         kind,
		ID:           id,
		ProjectIndex: projectIndex,
	}
}

func projectIndexByShorthand(shorthand string) int {
	for i, project := range projects {
		if shorthand == project.shorthand {
			return i
		}
	}
	return -1
}

func fetchJSON(url string, data interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP error: %v", resp.Status)
	}

	return json.NewDecoder(resp.Body).Decode(data)
}

func fetchGitlabProjectID(gitlabURL string, path string) (int64, error) {
	var data struct {
		ID int64 `json:"id"`
	}
	err := fetchJSON(gitlabURL+"/api/v4/projects/"+url.PathEscape(path), &data)
	return data.ID, err
}

type gitlabIssue struct {
	Title  string `json:"title"`
	WebURL string `json:"web_url"`
}

func fetchGitlabIssue(project gitlabProject, id string) (*gitlabIssue, error) {
	endpoint := fmt.Sprintf("%v/api/v4/projects/%v/issues/%v", project.rootURL, project.id, id)
	var data gitlabIssue
	err := fetchJSON(endpoint, &data)
	return &data, err
}

type gitlabMergeRequest struct {
	Title  string `json:"title"`
	WebURL string `json:"web_url"`
}

func fetchGitlabMergeRequest(project gitlabProject, id string) (*gitlabMergeRequest, error) {
	endpoint := fmt.Sprintf("%v/api/v4/projects/%v/merge_requests/%v", project.rootURL, project.id, id)
	var data gitlabMergeRequest
	err := fetchJSON(endpoint, &data)
	return &data, err
}

type gitlabCommit struct {
	ID     string `json:"id"`
	Title  string `json:"title"`
	WebURL string `json:"web_url"`
}

func fetchGitlabCommit(project gitlabProject, hash string) (*gitlabCommit, error) {
	endpoint := fmt.Sprintf("%v/api/v4/projects/%v/repository/commits/%v", project.rootURL, project.id, hash)
	var data gitlabCommit
	err := fetchJSON(endpoint, &data)
	return &data, err
}
