package main

import (
	"os"

	"git.sr.ht/~emersion/go-scfg"
)

type Config struct {
	Listen        string `scfg:"listen"`
	Connect       string `scfg:"connect"`
	GitlabProject []struct {
		Params []string `scfg:",param"`
	} `scfg:"gitlab-project"`
	GitlabToken string `scfg:"gitlab-token"`
}

func loadConfig(filename string) (*Config, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var cfg Config
	err = scfg.NewDecoder(f).Decode(&cfg)
	return &cfg, err
}
